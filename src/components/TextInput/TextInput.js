import React from 'react';
import styled from 'styled-components'

function TextInput({ placeholder }) {
    return (
        <label><span>this is label</span>
            <input disabled
                placeholder={ placeholder }
            />
        </label>
    )
}

export default TextInput;