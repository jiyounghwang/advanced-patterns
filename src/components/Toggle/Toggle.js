import React from 'react';
import './Toggle.css'

class Toggle extends React.Component {
    static defaultProps = {onToggle: () => {}}
    state = {on: false}

    // toggle = () => 
    //     // this.setState(updater[, callback])
    //     this.setState(({on}) => ({on: !on}),
    //     // (prevState, props) => stateChange
    //         () => {
    //             this.props.onToggle(this.state.on)
    //         }
    //     )
    
    toggle = () => {
        this.setState({ on: !this.state.on }, () => {
            this.props.onToggle(this.state.on)
        })
    }

    render() {
        const { on } = this.state
        return (
            <Switch on={on} onClick={this.toggle}/>
        )
    }
}

function Switch({ on, ...props }) {
    console.log(props)
    return (
        <div className="toggle">
        {/* <input className="toggle-input" type="checkbox" /> */}
        <div
            type="checkbox" 
            className={`toggle-btn ${on ? 'toggle-btn-on' : 'toggle-btn-off'}`}
            aria-expanded={on}
            {...props}
        />
        </div>
    )
}

export default Toggle;