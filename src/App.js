import React, { Component } from 'react';
import './App.css';
// import TextInput from './components/TextInput'
import { ThemeProvider } from 'styled-components'
import { Flex, Box } from 'grid-styled'
import Toggle from './components/Toggle'
import PasswordInput from './components/PasswordInput'
import TextInput from './components/TextInput'

class App extends Component {
  render() {
    return (
      <ThemeProvider
        theme={{
          space: [0, 8, 16, 24, 32],
          breakpoints: [ '320px', '768px', '1020px' ]
        }}>
        <Flex>
          <Box w={[1, 1/2, 1/3]} p={3}>
            <Toggle onToggle={on => console.log('toggle', on) }/>
            <PasswordInput/>
            <TextInput />
          </Box>
        </Flex>
      </ThemeProvider>
    );
  }
}

export default App;
